export const ExamsSchema = {
  name: 'Exams',
  properties: {
    _id: 'objectId?',
    userID: 'string?',
    courseName: 'string?',
    courseTopic: 'string?',
    date: 'date?',
    time: 'date?',
    icon: 'string?',
    notifications: 'NotificationsExams[]',
  },
  primaryKey: '_id',

};

export const NotificationsExamsSchema = {
  name: 'NotificationsExams',
  embedded: true,
  properties: {
    id: 'string?',
    date: 'date?',
  },
};
